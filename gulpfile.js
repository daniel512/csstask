const { src, dest } = require('gulp');
const sass =  require('gulp-sass')(require('sass'));
const cssConcat = require('gulp-concat-css');

function css() {
    return src(['css/*.css', 'sass/*.sass'])
        .pipe(sass())
        .pipe(cssConcat('style.css'))
        .pipe(dest('./dist/'));
}

exports.css = css;
